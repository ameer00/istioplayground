export PROJECT=$(gcloud info --format='value(config.project)')
export PROJECT_NUM=$(gcloud projects describe ${PROJECT} --format='value(projectNumber)')
export VPC='istio-vpc'
export REGION1='us-west2'
export ZONE1='us-west2-a'
export REGION1_SUBNET='10.0.4.0/22'
export SUBNET1_SVC_RANGE='10.0.32.0/20'
export SUBNET1_POD_RANGE='10.4.0.0/14'
export REGION2='us-central1'
export ZONE2='us-central1-a'
export REGION2_SUBNET='172.16.4.0/22'
export SUBNET2_SVC_RANGE='172.16.16.0/20'
export SUBNET2_POD_RANGE='172.20.0.0/14'
export GKE1='west'
export GKE2='central'
export ISTIO_VERSION='1.3.0-rc.1'
WORKDIR=$HOME/istioplayground
