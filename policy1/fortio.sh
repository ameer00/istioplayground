#! /bin/bash

kubectl exec -it $(kubectl get pod --context $1 -n $2 | grep fortio | awk '{ print $1 }') --context $1 -n $2 -c fortio -- fortio load -c 5 -qps 100 -t $3 $4