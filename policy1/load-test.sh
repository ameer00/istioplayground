#! /bin/bash

./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app1:8000/get &
./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app2:8000/get &
./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app3:8000/get &
./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app4:8000/get &
./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app3.global:8000/get &
./fortio.sh ${OPS_GKE_1} app1 5m httpbin.app4.global:8000/get &

./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app1:8000/get &
./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app2:8000/get &
./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app3:8000/get &
./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app4:8000/get &
./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app1.global:8000/get &
./fortio.sh ${OPS_GKE_2} app3 5m httpbin.app2.global:8000/get &

echo Fortio load tests running for 5 minutes...